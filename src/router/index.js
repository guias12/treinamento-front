// Import ZdPage component from Zeedhi plugin
import Router from 'vue-router';
import { ZdPage } from '@zeedhi/vue';

// Add Hello World route
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'treinamento',
      path: '/',
      component: ZdPage,
      props: {
        path: 'treinamento',
        local: true,
      },
    },
  ],
});