import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router';

import vuetify from './plugins/vuetify';

import Zeedhi from '@zeedhi/vue';
import ZeedhiComponents from '@zeedhi/vuetify';
import '@zeedhi/vuetify/dist/zd-style.css';

import router from './router';

Vue.config.productionTip = false

Vue.use(Zeedhi);
Vue.use(ZeedhiComponents);
Vue.use(VueRouter);

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
